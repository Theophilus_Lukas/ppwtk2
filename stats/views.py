from django.shortcuts import render
from .models import provinsi
from django.http import HttpResponse
from django.views.generic import ListView

# Create your views here.
class showprovinsiView(ListView):
    model = provinsi
    template_name = 'stats/face.html'
    ordering = ['-positif']