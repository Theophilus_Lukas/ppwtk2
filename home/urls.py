from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('data/', views.searchhospital, name="searchhospital"), #BintangNumpang
    path('feedback/', views.feedback, name="feedback"),
    path('masukkan/', views.masukkan, name='masukkan'),
]