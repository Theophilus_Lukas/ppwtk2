from django.test import TestCase,Client
from django.urls import reverse, resolve
from .views import index, searchhospital, masukkan, feedback
from .models import ShowSubmisi
from bintang.models import Cerita

# Create your tests here.
class HomeTest(TestCase):
    def setUp(self):
        response_1 = self.client.get('/')
        self.show_submisi = ShowSubmisi.objects.all()[0]
        c1 = Cerita(
            nama_pencerita='Kucing',
            email_pencerita='kucing@ui.ac.id',
            judul_pencerita='Aku kucing baik',
            cerita_pencerita='mantap',
            submisi=self.show_submisi
        )
        c1.save()
        c2 = Cerita(
            nama_pencerita='Anjing',
            email_pencerita='anjing@ui.ac.id',
            judul_pencerita='aku anjing',
            cerita_pencerita='wow',
            submisi=self.show_submisi
        )
        c2.save()

    def test_view_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_status_code_200_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_only_one_showsubmisi_model(self):
        response = self.client.get(reverse('home:index'))
        count= ShowSubmisi.objects.count()
        self.assertEqual(count, 1)

    def test_count_submitted_cerita_model(self):
        # membuat objek show_submisi pada response 1
        count = self.show_submisi.cerita_set.count()

        # refresh page
        response_2 = self.client.get('/')
        self.assertContains(response_2, 'Submisi #', count)
        self.assertContains(response_2, '<div class="swiper-submisi-wrapper"')

    def test_nav_bar(self):
        response = self.client.get('/')
        isi = response.content.decode('utf8')
        self.assertIn('<nav>', isi)
        self.assertIn('</nav>', isi)

    # tk 2
    def test_search_for_anon(self):
        response = self.client.get('/', {'s1': 'Aku'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('cerita' in response.json())
        self.assertIn('Aku kucing baik', response.json()['cerita'])

# Bintang Numpang
class TestSearchEvent(TestCase):
    def test_event_searchhospital_url_is_exist(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code, 200)

    def test_event_searchhospital_func(self):
        found = resolve('/data/')
        self.assertEqual(found.func, searchhospital)

class TestMasukkan(TestCase):
    def test_event_masukkan_url_is_exist(self):
        response = Client().get('/masukkan/')
        self.assertEqual(response.status_code, 200)

    def test_event_masukkan_func(self):
        found = resolve('/masukkan/')
        self.assertEqual(found.func, masukkan)

class TestFeedBack(TestCase):

    def test_event_feedback_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, feedback)