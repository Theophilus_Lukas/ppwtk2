
$(function () {
    $('.content').delay(2000).fadeIn(400);
    const endpoint = '/';
    var delay = false;

    let ajax_call = function (endpoint, req_parameters, pos) {
        $.getJSON(endpoint, req_parameters)
            .done(response => {
                let child;
                while (child = document.getElementById(`${pos}`).firstChild) {
                    child.remove();
                }
                $(`#${pos}`).append(response['cerita']);
                try {
                    submisiSwiper[0].update();
                } catch (error) {
                    if (error instanceof ReferenceError) {
                        submisiSwiper[0].update();
                    }
                }
            })
    };

    $('#id_sortby').on('change', function () {
        var keyword = $(this).children("option:selected").val();
        const par = {sort:keyword};

        if (delay) clearTimeout(delay);

        delay = setTimeout(ajax_call, 800, endpoint, par, 'first');
    });

    $('#id_search_1').on('keyup', function() {
        var keyword = $('#id_search_1').val()
        const par = {s1:keyword}

        if (delay) clearTimeout(delay);

        delay = setTimeout(ajax_call, 800, endpoint, par, 'first');
    });
});