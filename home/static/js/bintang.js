$(document).ready(function () {
    $("#keyword").on('keyup', function () {
        var keyword = $(this).val();
        $.ajax({
            url: "/data/",
            success: function (data) {
                var array = data;
                var obj_hasil = $("#hasil");
                obj_hasil.empty();
                for (index = 0; index < array.length; index++) {
                    if (keyword === "") {
                        obj_hasil.empty();
                    } else if (array[index].province.toLowerCase().includes(keyword.toLowerCase())) {
                        var nama_rumah_sakit = array[index].name;
                        var alamat = array[index].address;
                        var kontak = array[index].phone;
                        var nama_kota = array[index].region;
                        obj_hasil.append(
                            "<div class=" + "row" + ">" + 
                                "<div class=" + "col-sm-12" + ">" +
                                    "<div class=" + "card" + ">" +
                                        "<div class=" + "card-body" + ">" + 
                                            "<h5 class=" + "card-title" + ">"+ nama_rumah_sakit + "</h5>" +
                                            "<p>__________________________________________________________________________________________________</p>" + 
                                            "<div class=" + "card-text" + ">" + nama_kota +"</div>" +
                                            "<div class=" + "card-text" + ">" +  "Telp : " + kontak +"</div>" +
                                            "<div class=" + "card-text" + ">" +  "Alamat : " + alamat +"</div>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                            "</div>" + "<br></br>"
                        );
                    }

                };
            }
        });
    });

    $(document).on('submit', '#new_user_form', function() {
        $.ajax({
            type:'POST',
            url:'/feedback/',
            data: {
                nama_feedback:$("#nama_feedback").val(),
                feedback_pencerita:$("#feedback_pencerita").val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            },
            success:function() {

            }
        });
    });
});