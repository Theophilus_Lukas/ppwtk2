const tablet = window.matchMedia("(max-width: 768px)");

var mySwiper = new Swiper('.swiper-container', {
    // Optional parameters
    // direction: 'vertical',
    // loop: true,
  
    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  
    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  })

var submisiSwiper = new Swiper('.swiper-submisi', {
  setWrapperSize: true,
  spaceBetween: 20,
  slideClass: 'swiper-submisi-slide',
  wrapperClass: 'swiper-submisi-wrapper',
  observer: true,
  observeParents: true,
  observeSlideChildren:true,
  scrollbar: {
    el: '.swiper-submisi-scrollbar'
  },
  pagination: {
    el: '.swiper-submisi-pagination',
    clickable: true
  },
  breakpoints: {  
  
    // when window width is >= 320px     
    320: {       
      slidesPerView: 1,
      spaceBetween: 10     
    },     
    // when window width is >= 480px     
    800: {       
      slidesPerView: 2,       
      spaceBetween: 20     
    },
    1336: {
        slidesPerView: 3,
        spaceBetween: 20
    }
} 
})

