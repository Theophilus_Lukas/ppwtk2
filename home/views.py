from django.shortcuts import redirect, render
from django.http import JsonResponse
from django.template.loader import render_to_string
from .models import ShowSubmisi, FeedBack
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.functions import Lower

#BintangNumpang
from django.http import JsonResponse
from django.http import HttpResponse
import requests
import json
# Create your views here.

def index(request):


    if ShowSubmisi.objects.all().count() == 0:
        submisi = ShowSubmisi.objects.create(id=1)
    else:
        count = ShowSubmisi.objects.count()
        for i in range(1,count):
            ShowSubmisi.objects.all()[i].delete()
        submisi = ShowSubmisi.objects.all()[0]
        ShowSubmisi.objects.filter(id=submisi.id).update(id=1)

    if request.is_ajax():
        data = ''
        # Search
        if 's1' in request.GET:
            keyword = request.GET['s1']
            data = submisi.cerita_set.filter(judul_pencerita__istartswith=keyword)
        # Sort
        elif 'sort' in request.GET:
            keyword = request.GET['sort']    
            # berdasarkan judul
            if keyword == '0':
                data = submisi.cerita_set.order_by(Lower('judul_pencerita'))
            # berdasarkan tanggal (recently)
            if keyword == '1':
                data = submisi.cerita_set.order_by('-published')
        html = render_to_string(
            template_name='home/ajax_card.html',
            context={'ceritas':data}
        )

        return JsonResponse(data={'cerita':html})

    cerita_user_logged_in = None
    if request.user.is_authenticated :
        try:
            cerita_user_logged_in = submisi.cerita_set.filter(email_pencerita=request.user.email)
        except ObjectDoesNotExist:
            cerita_user_logged_in = None
    # initialize
    cerita = submisi.cerita_set.order_by('-published')
    context = {
        'ceritas': cerita, 'cerita_user':cerita_user_logged_in
    }
    return render(request, 'home/index.html', context)


#BintangNumpang
def searchhospital(request):
    url = 'https://dekontaminasi.com/api/id/covid19/hospitals/'
    retrive = requests.get(url)
    data = json.loads(retrive.content)
    return JsonResponse(data, safe=False)
    
def feedback(request):
    if request.method == "POST":
        nama_feedback = request.POST['nama_feedback']
        feedback_pencerita = request.POST['feedback_pencerita']

        FeedBack.objects.create(
            nama_feedback = nama_feedback,
            feedback_pencerita = feedback_pencerita,
        )

        return redirect('home:index')

def masukkan(request):
    masukkan = FeedBack.objects.all()
    context = {
        'masukkan' : masukkan,
    }
    return render(request, 'home/masukkan.html', context)