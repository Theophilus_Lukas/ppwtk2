# Project Covid
[![pipeline status][pipeline-badge]][commits-gl]

[ProjectSaveUs][link-web]

[link-web]: http://projectsaveus.herokuapp.com/

## Anggota

- Theophilus Lukas / 1906398332
- Bintang Samudro / 1906399410
- Mohammad Hafizh Dwiandra Ramadhan / 1906398383
- Bima Bagas Raditya / 1906398585
- Deija Shakina Ayu / 1906399373

[pipeline-badge]: https://gitlab.com/Theophilus_Lukas/project-covid/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/Theophilus_Lukas/project-covid/-/commits/master

## Cerita aplikasi
Aplikasi yang kami buat adalah website khusus tentang Covid-19 yang berisi informasi yang nantinya berguna untuk masyarakat. 

Website ini nantinya bisa untuk digunakan oleh user sendiri untuk memberikan informasi atau pengalaman tersendiri yang berhubungan dengan Covid-19 baik berupa, angka penyebaran di suatu daerah maupun sebuah artikel. 

Tujuan dari dibuatnya website ini adalah untuk menyebarkan informasi agar semakin waspada untuk menjaga kesehatan dan tidak sembarangan pergi ke tempat yang berada di Zona Merah.

## Fitur
Total Halaman 5 : 
- Home page : Informasi atau statistik penyebaran Covid 19 di suatu     daerah dan artikel-artikel yang informatif (Baca Selengkapnya) (Bima Bagas)
- Page Tindakan Preventif : Beberapa informasi atau tips untuk menjaga diri dari Covid-19 (Deija)
- Form Submisi Cerita : User bisa memasukkan berita atau pengalaman sendiri yang nantinya akan bisa dibaca oleh pengguna lainnya. (Bintang) 
- Halaman Tabel Statistik penyebaran Covid-19 : User bisa melihat statistik penyebaran Covid-19 di 5 Provinsi yang penyebarannya tertinggi. (Lukas)
- Halaman detail dari suatu provinsi : User bisa melihat statistik penyebaran Covid-19 secara detail dari masing2 provinsi (Hafizh)

## Kumpulan link
- [Persona][link-persona]
- [Wireframe][link-wireframe] 
- [Mockup][link-mockup]

[link-persona]: https://docs.google.com/document/d/1bNsN1HbP_t-r9aUJvZIrgLCCi7AJKkfmCMuebOyi33Q/edit?usp=sharing]
[link-wireframe]: https://www.figma.com/file/qhZlw1UZCktjSPOSEGBesj/Wireframe?node-id=0%3A1
[link-mockup]: https://www.figma.com/file/mKH4vzPDp6mafMJMTouSOe/Mock-up