from django.http import request
from django.shortcuts import render
from .models import detilprov
from stats.models import provinsi
# Create your views here.

def detil(request, pk):
    dataA = provinsi.objects.get(id = pk)
    dataP = detilprov.objects.all()
    context = {'dataP' : dataP, 'dataA': dataA}
    return render(request, 'detil.html', context)
