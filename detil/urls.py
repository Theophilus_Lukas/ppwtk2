from django.urls import path
from . import views

app_name = 'detil'

urlpatterns = [
    path('<int:pk>', views.detil, name='detil'),
]