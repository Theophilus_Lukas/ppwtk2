from django.test import TestCase, Client
from stats.models import provinsi
from .models import detilprov

# Create your tests here.
class TestingDetil(TestCase):
    def test_url_stats(self):
        provtst = provinsi.objects.create(nama="test", mati="0", positif="0", sembuh="0")
        detilprov.objects.create(namaProv=provtst, laki="0", perempuan="0", lainjk="0", usia1="0", usia2="0", usia3="0", usia4="0", usia5="0", usia6="0", lainusia="0")
        response = Client().get('/detil/1')
        self.assertEquals(response.status_code, 200)