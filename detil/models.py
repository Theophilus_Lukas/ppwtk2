from django.db import models
from stats.models import provinsi
# Create your models here.

class detilprov(models.Model):
    namaProv = models.ForeignKey(provinsi, on_delete=models.CASCADE, max_length = 50, null= True, blank=False)
    laki = models.CharField(max_length=50, default="0")
    perempuan = models.CharField(max_length=50, default="0")
    lainjk = models.CharField(max_length=50, default="0")
    usia1 = models.CharField(max_length=50, default="0")
    usia2 = models.CharField(max_length=50, default="0")
    usia3 = models.CharField(max_length=50, default="0")
    usia4 = models.CharField(max_length=50, default="0")
    usia5 = models.CharField(max_length=50, default="0")
    usia6 = models.CharField(max_length=50, default="0")
    lainusia = models.CharField(max_length=50, default="0")