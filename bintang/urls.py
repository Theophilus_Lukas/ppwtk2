from django.urls import path
from django.conf.urls import url 

from . import views

app_name = 'bintang'

urlpatterns = [
    path('submisi/', views.submisi, name='submisi'),
    path('detail/<int:detail_id>', views.detail, name='detail'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
    
]