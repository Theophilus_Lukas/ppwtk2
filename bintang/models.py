from django.db import models
from home.models import ShowSubmisi
# Create your models here.

class Cerita(models.Model):
    nama_pencerita = models.CharField(max_length = 50)
    email_pencerita = models.EmailField(max_length = 50)
    judul_pencerita = models.CharField(max_length = 50, null=True)
    cerita_pencerita = models.TextField(blank=False, null=False)
    published = models.DateTimeField(auto_now_add= True)
    # Hubungkan cerita dengan submisi pada app 'home'
    submisi = models.ForeignKey(ShowSubmisi, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama_pencerita)