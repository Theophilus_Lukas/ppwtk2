from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.

from .views import submisi, detail, delete
from .models import Cerita

class TestDetail(TestCase):
    def setUp(self):
        cerita = Cerita(nama_pencerita="Bintang", email_pencerita="bintangnaxbonlap@gmail.com", judul_pencerita="Corona Ini Rasanya", cerita_pencerita="lorem ipsum")
        cerita.save()

    def test_detail_url_is_exist(self):
        response = Client().get('/bintang/detail/1')
        self.assertEqual(response.status_code, 200)

    def test_detail_index_func(self):
        found = resolve('/bintang/detail/1')
        self.assertEqual(found.func, detail)

    def test_detail_using_template(self):
        response = Client().get('/bintang/detail/1')
        self.assertTemplateUsed(response, 'home/detail.html')

    # Tahap 2
    def test_tombol_hide_show_in_detail(self):
        response = Client().get('/bintang/detail/1')
        isi = response.content.decode('utf8')
        self.assertIn('<button id="hide" class="btn btn-danger">Sembunyikan Cerita dari ', isi)
        self.assertIn('<button id="show" class="btn btn-primary" style="display: none">Tampilkan Cerita dari ', isi)

class TestTambahCerita(TestCase):
    def test_tambah_hasil_url_is_exist(self):
        response = Client().get('/bintang/submisi/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_hasil_index_function(self):
        found = resolve('/bintang/submisi/')
        self.assertEqual(found.func, submisi)

    def test_tambah_hasil_using_template(self):
        response = Client().get("/bintang/submisi/")
        self.assertTemplateUsed(response, 'home/submisi.html') 

    def test_submisi_model_create_new_object(self):
        cerita = Cerita(nama_pencerita="Bintang", email_pencerita="bintangnaxbonlap@gmail.com", judul_pencerita="Corona Ini Rasanya", cerita_pencerita="lorem ipsum")
        cerita.save()
        self.assertEqual(Cerita.objects.all().count(), 1)

    def test_submisi_url_post_is_exist(self):
        response = Client().post('/', data={'nama_pencerita':'Bintang', "email_pencerita":"bintangnaxbonlap@gmail.com", "judul_pencerita":"Corona Ini Rasanya", "cerita_pencerita":"lorem ipsum"})
        self.assertEqual(response.status_code, 200)

# Tahap 2
class TestBelomLogin(TestCase):
    def testNotLogin(self):
        response = Client().get('/bintang/submisi/')
        isi = response.content.decode('utf8')
        self.assertIn('<h1>Harap login terlebih dahulu</h1>', isi)

class TestHapusCerita(TestCase):
    def setUp(self):
        cerita = Cerita(nama_pencerita="Bintang", email_pencerita="bintangnaxbonlap@gmail.com", judul_pencerita="Corona Ini Rasanya", cerita_pencerita="lorem ipsum")
        cerita.save()

    def test_hapus_url_is_exist(self):
        response = Client().get('/bintang/delete/1')
        self.assertEqual(response.status_code, 302)

    def test_delete_index_func(self):
        found = resolve('/bintang/delete/1')
        self.assertEqual(found.func, delete)

    def test_delete_model_delete_exist_object(self):
        Cerita.objects.get(nama_pencerita="Bintang").delete()
        self.assertEqual(Cerita.objects.all().count(), 0)