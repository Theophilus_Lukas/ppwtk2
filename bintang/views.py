from django.shortcuts import render, redirect
from .forms import PostForm
from .models import Cerita
from home.models import ShowSubmisi
# Create your views here.

def submisi(request):
    post_form = PostForm(request.POST or None)

    if request.method == 'POST':

        if post_form.is_valid():
            
            post_form = post_form.save(commit=False)
            post_form.submisi = ShowSubmisi.objects.all()[0]
            post_form.save()
            return redirect('home:index')

    context = {
        'page_title':'Masukkan Ceritamu',
        'post_form':post_form,
    }
    return render(request, 'home/submisi.html', context)

def detail(request, detail_id):
    id_cerita = Cerita.objects.get(id=detail_id)
    context = {
        'cerita' : id_cerita
    }
    return render(request, 'home/detail.html', context)

def delete(request, delete_id):
    Cerita.objects.filter(id=delete_id).delete()
    return redirect('home:index')