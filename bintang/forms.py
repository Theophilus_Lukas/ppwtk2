from django import forms


# import model dari models.py
from .models import Cerita


class PostForm(forms.ModelForm):
    class Meta:
        model = Cerita
        fields = [
            'nama_pencerita',
            'email_pencerita',
            'judul_pencerita',
            'cerita_pencerita',
        ]

        labels = {
            'nama_pencerita' : 'Nama Depan',
            'email_pencerita' : 'Email',
            'judul_pencerita' : 'Judul',
            'cerita_pencerita' : 'Masukan Ceritamu',
        }

        widgets = {
            'nama_pencerita': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Bintang Samudro',
                    
                }
            ),
            'email_pencerita': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: bintangnaxbonlap@gmail.com',
                    
                }
            ), 
            'judul_pencerita': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Rasanya Corona Sangat ....',
                }
            ),
            'cerita_pencerita': forms.Textarea(
                attrs= {
                    'class':'form-control',
                    'placeholder':'Contoh: Pada hari ini aku .....',
                }
            ),

        }
