$(document).ready(function () {
    $("#cerita_pencerita").on('keyup', function () {
        var myText = $("#cerita_pencerita").val().replace(/\s+/g, '');
        $('#alphabetCount').text(myText.length);
    });

    $("input").click(function () {
        $(this).css("background-color", "#D8F2FF");
    });

    $("textarea").click(function () {
        $(this).css("background-color", "#D8F2FF");
    });

    $("input").blur(function () {
        $(this).css("background-color", "white");
    });

    $("textarea").blur(function () {
        $(this).css("background-color", "white");
    });

    $("#hide").css("width", "100%");

    $("#hide").click(function () {
        $(".container-paragraph").slideUp("slow");
        $(this).css("display", "none");
        $("#show").css("display", "inline");
        $("#show").css("width", "100%");
    });
    $("#show").click(function () {
        $(".container-paragraph").slideDown("slow");
        $("#hide").css("display", "inline");
        $(this).css("display", "none");
    });

});