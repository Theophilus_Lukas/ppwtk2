from django.urls import path
from . import views
from .views import betterStatsView


app_name = 'betterstats'

urlpatterns = [
    path('', betterStatsView, name='betterstats'),
]