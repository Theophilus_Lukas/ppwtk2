from django.test import TestCase, Client
from .views import betterStatsView
from django.urls import resolve

# Create your tests here.
class TestingBetterStats(TestCase):
    def test_url_responds(self):
        response = Client().get('/betterstats/')
        self.assertEquals(response.status_code, 200)

    def test_view_search_country(self):
        found = resolve('/betterstats/')
        self.assertEqual(found.func, betterStatsView)

    def test_template_search_country(self):
        response = Client().get('/betterstats/')
        html_back = response.content.decode('utf8')
        self.assertIn("</select>", html_back)
        self.assertIn("results_list_show", html_back)
        self.assertIn("Search Statistics by Country", html_back)

