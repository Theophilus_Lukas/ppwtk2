from django.apps import AppConfig


class BetterstatsConfig(AppConfig):
    name = 'betterstats'
