from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def betterStatsView(request):
    response = {}
    return render(request, 'betterstats/betterstats.html', response)