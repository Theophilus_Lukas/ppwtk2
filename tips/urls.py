from django.urls import path
from . import views

app_name = 'tips'

urlpatterns = [
    path('cuci-tangan/', views.cuci_tangan, name='cuci_tangan'),
    path('di-rumah/', views.di_rumah, name='di_rumah'),
    path('di-luar-rumah', views.di_luar_rumah, name='di_luar_rumah'),
    path('', views.add_jurnal, name='add_jurnal'),
    path('view-jurnal/', views.view_jurnal, name='view_jurnal'),
    
    
]