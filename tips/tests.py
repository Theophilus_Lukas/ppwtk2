from django.test import TestCase,LiveServerTestCase, tag, Client
from .models import Checkin
from .forms import CheckinForm
from .views import add_jurnal, view_jurnal, cuci_tangan, di_rumah, di_luar_rumah
from django.http import HttpRequest
from django.urls import reverse, resolve
from selenium import webdriver
# Create your tests here.
class TestCheckinUnitTest(TestCase):

    def test_checkin_url_is_exist(self):
        response = Client().get("/tips/")
        self.assertEquals(200, response.status_code)

    def test_template_form(self):
        response = Client().get("/tips/")
        self.assertTemplateUsed(response, "edukasi.html")
    
    # def test_template_form1(self):
    #     response = Client().get("/tips/view-jurnal")
    #     self.assertTemplateUsed(response, "view_jurnal.html")

    # def test_template_form2(self):
    #     response = Client().get("/tips/cuci-tangan")
    #     self.assertTemplateUsed(response, "cuci_tangan.html")
    
    # def test_template_form3(self):
    #     response = Client().get("/tips/di-rumah")
    #     self.assertTemplateUsed(response, "di_rumah.html")
    
    def test_template_form4(self):
        response = Client().get("/tips/di-luar-rumah")
        self.assertTemplateUsed(response, "di_luar_rumah.html")
    
    # def test_view_form(self):
    #     response = Client().get("/tips/")
    #     html_response = response.content.decode("utf8")
    #     self.assertIn('<h5 class="judul">Edukasi Covid-19</h5>',html_response)
    #     self.assertIn('<form method="POST">', html_response)
    
    def test_model_create(self):
        Checkin.objects.create(Subject="coba")
        Checkin.objects.create(Date="coba")
        Checkin.objects.create(Message="hai")
        count = Checkin.objects.all().count()
        self.assertEquals(count,3)

    def test_model_name(self):
        Checkin.objects.create(Subject='check')
        checkin = Checkin.objects.get(Subject='check')
        self.assertEqual(str(checkin),'Checkin object (1)')

    def test_form_validation_accepted(self):
        form = CheckinForm(data={'checkin': 'test'})
        self.assertFalse(form.is_valid())
    

@tag('functional')
class IndexFunctionalTest(LiveServerTestCase):
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
