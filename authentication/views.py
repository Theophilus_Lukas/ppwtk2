from django.shortcuts import render, redirect
from django.contrib.auth import authenticate,login, logout
from django.contrib import messages

from .forms import RegisterForm

# Create your views here.
def login_view(request):
    context = {}
    username = request.POST.get('username')
    password = request.POST.get('password')
    username_global = username
    if request.method == 'POST':
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            request.session['user_id'] = username+password
            return redirect('/')
        else:
            messages.error(request, 'Username atau password yang anda masukkan salah!')
    return render(request, 'authent/login.html', context)

def signup(request):
    form = RegisterForm()
    if request.method == 'POST':   
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Akun berhasil dibuat untuk ' + username)
            return redirect('/authe/')
    
    context = {'form' : form}
    return render(request, 'authent/signup.html', context)
    
def logout_view(request):
    logout(request)
    # del request.session['user_id']
    return redirect('/authe/')