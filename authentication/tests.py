from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
# Create your tests here.

class AuthViewTest(TestCase):
    pass

class AuthTest(TestCase):
    def setUp(self):
        self.credentials = {
            'first_name': 'PPW TK 2',
            'username' : 'pepewTeka2',
            'email' : 'tk2g11@gmail.com',
            'password' : 'tk2pepewauthentication',
        }
        User.objects.create_user(**self.credentials)

    def test_signup(self):
        self.assertEqual(User.objects.count(), 1)

    def test_authenticate(self):
        c2 = authenticate(**self.credentials)
        self.assertTrue(c2.username == self.credentials['username'])

    def test_login_password_is_ok(self):
        # coba login, pastiin view udh ada method login()
        response = self.client.post('/authe/', {
                'username': self.credentials['username'],
                'password': self.credentials['password']
        })
        
        # cek sessionnya ada atau ngga
        session = self.client.session
        self.assertTrue(session['user_id'])

    def test_logout(self):
        self.client.logout()
        # cek sessionnya
        session = self.client.session
        self.assertFalse('user_id' in session)

