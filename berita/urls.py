from django.urls import path
from . import views
from .views import showBeritaViews, search_news, beritaSearchView
from django.views.decorators.csrf import csrf_exempt

app_name = 'berita'

urlpatterns = [
    path('', showBeritaViews.as_view(), name='beritahome'),
    path('beritasearch/', beritaSearchView, name='beritasearch'),
    path('searchresults/', csrf_exempt(search_news), name='search-results'),
]