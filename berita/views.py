from django.shortcuts import render
from django.http import JsonResponse
from .models import berita
from django.views.generic import ListView
import json


# Create your views here.

class showBeritaViews(ListView):
    model = berita
    template_name = 'berita/berita.html'

def beritaSearchView(request):
    response = {}
    return render(request, 'berita/beritaSearch.html', response)

def search_news(request):
    search_str = request.GET['q']

    news = berita.objects.filter(judul__icontains=search_str)

    data = news.values()
    return JsonResponse(list(data), safe=False)
