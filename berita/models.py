from django.db import models
from datetime import datetime, date

# Create your models here.
class berita(models.Model):
    judul = models.CharField(max_length=80)
    penulis = models.CharField(max_length=150)
    preview = models.TextField(blank=False, null=False, default="berita here")
    sumber = models.TextField(blank=False, null=False, default="https://www.instagram.com/lukas.pictures/")
    tanggal = models.DateField(default="2001-01-17")