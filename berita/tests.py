from django.test import TestCase, Client
from .models import berita
from django.urls import resolve
from .views import showBeritaViews
#Create your tests here.
class TestingBerita(TestCase):
    def test_url_berita(self):
        response = Client().get('/berita/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_beritasearch(self):
        response = Client().get('/berita/beritasearch/')
        self.assertEquals(response.status_code, 200)

    def test_url_searchresults(self):
        response = Client().get('/berita/searchresults/?q=lukas')
        self.assertEquals(response.status_code, 200)

    def test_buat_berita(self):
        berita.objects.create(judul="test", penulis="test", preview="test", sumber="test")
        jumlahkegiatan = berita.objects.all().count()
        self.assertEquals(jumlahkegiatan, 1)

    def test_views_berita(self):
        found = resolve('/berita/')
        self.assertEquals(found.func.view_class , showBeritaViews)

    def test_template_html_berita(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'berita/berita.html')

    def test_json_berita_getter_exist(self):
        berita.objects.create(judul='this_is_just_a_test', penulis="tester")
        response = Client().get('/berita/searchresults/?q=')
        self.assertEquals(response.status_code, 200)

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            [
                {'id': 1, 'judul': 'this_is_just_a_test', 'penulis': 'tester', 'preview': 'berita here', 'sumber': 'https://www.instagram.com/lukas.pictures/', 'tanggal': '2001-01-17'}
            ]
        )
